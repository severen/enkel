# Enkel - Et enkelt tema for ZSH. / A simple theme for ZSH.
#
# This is free and unencumbered software released into the public domain.

enkel_lambda() {
    # Green if exit status is normal, red if not.
    echo -n "%(?,%{$fg[green]%}λ,%{$fg_bold[red]%}λ)"
    echo -n "%{$reset_color%} "
}

# The part of the prompt that displays the current user.
# The username is red if the current user is root, otherwise
# it is yellow.
enkel_user() {
    if [[ $USER == 'root' ]]; then
        echo -n "%{$fg[red]%}"
    else
        echo -n "%{$fg[blue]%}"
    fi
    echo -n "%n"
    echo -n "%{$reset_color%}"
}

# The part of the prompt that displays the hostname.
# The hostname will not display if it is a local connection.
enkel_host() {
    if [[ -n $SSH_CONNECTION ]]; then
      echo -n " %{$fg_bold[cyan]%}at%{$reset_color%} "
      echo -n "%{$fg[green]%}%m%{$reset_color%}"
      echo -n " %{$fg_bold[cyan]%}in%{$reset_color%} "
    else
      echo -n " %{$fg_bold[cyan]%}in%{$reset_color%} "
    fi
}

# The part of the prompt that displays the current directory.
# The directory will be red if there is not write permission.
enkel_current_dir() {
    echo -n "%{$fg[yellow]%}[%{$reset_color%}"
    if [[ ! -w $PWD ]]; then
        echo -n "%{$fg_bold[red]%}%~%{$reset_color%}"
    else
        echo -n "%{$fg_bold[blue]%}%~%{$reset_color%}"
    fi
    echo -n "%{$fg[yellow]%}]%{$reset_color%}"
}

enkel_git() {
    echo -n "$(git_prompt_info)"
    echo -n "$(git_prompt_status)"
}

# Build the left side/main prompt.
enkel_build_prompt() {
    enkel_lambda
    enkel_user
    enkel_host
    enkel_current_dir
    enkel_git
    echo -n "\n➜ "
}

# Build the right side prompt.
enkel_build_rprompt() {
    echo -n "%{$fg[yellow]%}[%{$reset_color%}"
    # Git revision.
    echo -n "$(git_prompt_short_sha)"
    echo -n "%{$fg_bold[blue]%}%*%{$reset_color%}" # Current time.
    echo -n "%(?..; %{$fg_bold[red]%}%?)%{$reset_color%}" # Exit status.
    echo -n "%{$fg[yellow]%}]%{$reset_color%}"
}

# Format for git_prompt_info().
ZSH_THEME_GIT_PROMPT_PREFIX=" %{$fg_bold[cyan]%}on%{$reset_color%} %{$fg[blue]%} "
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY=""
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[green]%} ✔"

# Format for git_prompt_status().
ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[green]%}+%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[blue]%}!%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%}-%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[magenta]%}>%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[yellow]%}#%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[cyan]%}?%{$reset_color%}"

# Format for git_prompt_long_sha() and git_prompt_short_sha().
ZSH_THEME_GIT_PROMPT_SHA_BEFORE="%{$fg_bold[blue]%}"
ZSH_THEME_GIT_PROMPT_SHA_AFTER="%{$reset_color%}; "

PROMPT='$(enkel_build_prompt)'
RPROMPT='$(enkel_build_rprompt)'
